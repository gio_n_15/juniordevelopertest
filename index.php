
<?php
include("view/header.php");

// include('db/database_connection.php');
include("./controllers/productsController.php");


?>
    <h1 class="float-left border-bottom mb-1">Products</h1>



        <div class="d-flex justify-content-end">
            <div class="col-auto my-1">
                <select class="custom-select mr-sm-2" id="inlineFormCustomSelect">
                    <option selected value="del" >mass delete action</option>
                    <option value="unmark">unmark all</option>
                </select>
            </div>

            <div class="col-auto my-1">
                <button type="submit" id="apply" class="btn btn-warning apply">Apply</button>
            </div>
        </div>


        <div class="row"  >
            <?php

                $result=[];
                $result=$product->act();

                foreach($result as $row){
               echo '             
                <div class="col-sm-3 mt-3">
                    <div class="card border-bottom" >

                        <div class="form-check ml-2">
                            <input class="form-check-input " type="checkbox" name="checkbox"   value="'.$row['id'].'">

                        </div>

                        <div class="card-body ">
                            <h5 class="card-title text-center">'.$row['SKU'].'</h5>
                            <p class="card-text text-center">'.$row['name'].'.</p>
                            <p class="card-text text-center">'.$row['price'].'$</p>
                            <p class="card-text text-center">'.$row['details'].'</p>
                        </div>
                    </div>
                </div>';
                }
            ?>
        </div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

 <script src="./js/main.js"></script>
</body>

</html>