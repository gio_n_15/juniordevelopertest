<?php
$operation="";
if(isset($_POST["action"])){
    $operation=$_POST["action"];
    // print_r( json_decode($_POST["data"]));
    
}


$product=new productsController($operation);
$product->act();
class productsController{
    private $operation;
    private $connect;
    private $sku;
    private $name;
    private $price;
    private $details;
    private $messages=[];
    
    function __construct($operation) {

        $this->connect = new PDO("mysql:host=localhost;dbname=data", "root", "");

        $this->operation=$operation;
    }
    public function getOperation(){

        return $this->operation;
    }
    public function act(){


        if($this->operation=="insert"){
            $this->insert();
        }elseif($this->operation=="delete"){
            
            $this->delete();
        }else{
           return $this->index();
            
        }
    }
    public function delete(){
      
            if($this->operation)
            {

            for($count = 0; $count < count($_POST["data"]); $count++)
            {
            $query = "DELETE FROM products WHERE id = '".$_POST['data'][$count]."'";
            $statement = $this->connect->prepare($query);
            $statement->execute();
            
            }
}

    }
    public function insert(){

        $input="";
        if ($this->operation) {
        $input=(array)json_decode($_POST["data"]);
        $this->sku=$input["sku"];

        $this->name = $input['name'];

        $this->price=$input['price'];
        $this->checkNum($this->price);

        if(isset($input['size'])){
        $this->checkNum($input['size']);
        $this->details = $input['size'].' (mb)';

        }elseif(isset($input['weight'])){
        $this->checkNum($input['weight']);
        $this->details = $input['weight'].' (kg)';
        }else{
        $this->checkNum([$input['height'],$input['width'],$input['lenght']]);
        $this->details = "H/W/L - ".$input['height'].":".$input['width'].":".$input['lenght'];
        }
        
        
     


        $query = "INSERT INTO `products`(`SKU`, `name`, `price`, `details`) VALUES (:sku,:name,:price,:details)";
        $statement = $this->connect->prepare($query);
        $statement->execute(array(
        ":sku" => $this->sku,
        ":name" => $this->name,
        ":price" => $this->price,
        ":details" => $this->details

        ));
        if(isset($this->messages['title'])){
            if($this->messages['title']=="error"){
                echo json_encode($this->messages);


            }
        }
       else{
            if($statement){

                $this->messages['title']="Inserted";
                $this->messages['body']='The data is saved';

                echo json_encode($this->messages);
                unset($this->messages);


            }
       }



  

}

    }
    public function index(){
            $query = "SELECT * FROM products ORDER BY id DESC";

            $statement = $this->connect->prepare($query);

            $statement->execute();

            return $statement->fetchAll(PDO::FETCH_ASSOC);
            


    }
    public  function checkNum($inputNum){
        if(!isset($this->messages['body'])){
            $this->messages['body']="";
        }
        if(is_array($inputNum)){
            foreach($inputNum as $val){
                if(!is_numeric($val)){

                $this->messages['title']="error";
                $this->messages['body']=$this->messages['body'].'<h3>'.$val.' not valid input </h3>';     
                }
            }   
        }else{
            if(!is_numeric($inputNum)){
                
                $this->messages['title']="error";
               
                $this->messages['body']=$this->messages['body'].'<h3>'.$inputNum.' not valid input </h3>';    
            }
        }
    }

} 