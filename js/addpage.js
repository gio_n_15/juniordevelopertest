$(document).ready(function () {
    const inputModes = [

        '<div class="input-group mb-3">\
    <div class="input-group-prepend">\
        <span class="input-group-text" id="basic-addon1">Size</span>\
    </div>\
    <input type="text" class="form-control" name="size">\
    </div> ',
        '<div class="input-group mb-3">\
        <div class="input-group-prepend">\
            <span class="input-group-text" id="basic-addon1">Height</span>\
        </div>\
        <input type="text" class="form-control" name="height"">\
    </div>\
    <div class="input-group mb-3">\
        <div class="input-group-prepend">\
            <span class="input-group-text" id="basic-addon1">Width</span>\
        </div>\
        <input type="text" class="form-control" name="width">\
    </div>\
    <div class="input-group mb-3">\
        <div class="input-group-prepend">\
            <span class="input-group-text" id="basic-addon1">Lenght</span>\
        </div>\
        <input type="text" class="form-control" name="lenght">\
    </div>\
    ',

        '<div class="input-group mb-3">\
            <div class="input-group-prepend">\
                <span class="input-group-text" id="basic-addon1">Weight</span>\
            </div>\
            <input type="text" class="form-control" name="weight">\
        </div>'
    ]
    $('select').on('change', function () {

        $('.selectedInput').html(inputModes[this.value])
    });
    $('.saveBtn').click(function () {
        let insert_data = {};
        let err ="";
        $('input').each(function () {
            let inpVal = $(this);

            if (!inpVal.val()) {
                err += `<h3>${inpVal.attr('name')} is emty </h3>`;
                inpVal.addClass("error")
            }else{
                inpVal.removeClass("error")
            }   
                
                insert_data[inpVal.attr('name')] = inpVal.val();
               
            });
            
        
        if (err) {
            $('.modal-body').html(err);
            $('#Modal').modal('show')
        }else{

            insert_data=JSON.stringify(insert_data);
            
          
            $.ajax({
                url:"./controllers/productsController.php",
                method:"POST",
                
                data: {data: insert_data,action:'insert'},
                daaType:'json',
                success:function(data)
                {
                    data=JSON.parse(data);
                    $('.modal-title').html(data.title);
                    
                     $('.modal-body').html(data.body);
                   
                    
                    $('#Modal').modal('show');
                    console.log(data)
                    if(data.title=="Inserted"){
                        console.log($('.form-control'))
                        $('.form-control').not('#exampleFormControlSelect1').val('')
                    }
         
                },
                
            })
        }
    })
});


