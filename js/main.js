
$(document).ready(function(){ 

    $('.form-check-input').click(function (){
        removeRowClass($(this))
        });

    $('#apply').click(function(){

        if($('#inlineFormCustomSelect').val()=="unmark")
        {
            $('.form-check-input').each(function (){
                this.checked=false;
                removeRowClass($(this))
                })
                return;
        }

        var checkbox = $('.form-check-input:checked');
        if(checkbox.length > 0)
        {
            var checkbox_value = [];
            $(checkbox).each(function(){
                checkbox_value.push($(this).val());
            });
  
            $.ajax({
                url:"./controllers/productsController.php",
                // url:"test.php",
                method:"POST",
                data:{data:checkbox_value,action:"delete"},
                success:function(msg)
                {
                   $.when( $('.removeRow').fadeOut(1000)).done(function (){
                    $(this).parent().remove();
                  
                   }
                   )
                }
            });   
        }
        else
        {
            alert("Select atleast one records");
        }
    });


});  
function removeRowClass(check){

        if(check.is(':checked'))
        {
           
            check.closest('.card').addClass('removeRow');
        }
        else
        {
            check.closest('.card').removeClass('removeRow');
        }
    }
