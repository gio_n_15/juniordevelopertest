<?php
include("./view/header.php");
?>
<div class="row mt-3">

    <h1 class="mr-auto">Product Add </h1>

</div>
<div class="inputs ml-5 mt-4">
    <div class="input-group mb-3">
        <div class="input-group-prepend">
            <span class="input-group-text" id="basic-addon1" >SKU</span>
        </div>
        <input type="text" class="form-control" name="sku">
    </div> 
    <div class="input-group mb-3">
        <div class="input-group-prepend">
            <span class="input-group-text" id="basic-addon1" >Name</span>
        </div>
        <input type="text" class="form-control" name="name">
    </div>
    <div class="input-group mb-3">
        <div class="input-group-prepend">
            <span class="input-group-text">Price</span>
        </div>
        <input type="text" class="form-control " name="price">
        <div class="input-group-append">
            <span class="input-group-text">$</span>
        </div>
    </div>

</div>
<div class="dynamicInputs">
    <div class="input-group mb-3 mt-1 selectType">
        <select class="form-control selectBox" name='select' id="exampleFormControlSelect1">
            <option value="0">1</option>
            <option value="1">2</option>
            <option value="2">3</option>
        </select>
    </div>
    <div class="d-flex">
        <div class="selectedInput ">
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="basic-addon1">Size</span>
                </div>
                <input type="text" class="form-control" name="size">
            </div>
        </div>
        <button type="button" class="btn btn-dark ml-auto mr-4 saveBtn">save</button>
    </div>
    <!-- 
        -->
</div>
<!-- Button trigger modal -->
<!-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
  Launch demo modal
</button> -->

<!-- Modal -->
<div class="modal fade" id="Modal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>

      </div>
    </div>
  </div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>

<script src="./js/addPage.js"></script>